//bai9
class Product {
  constructor(id, name, categoryId, saleDate, qulity, isDelete) {
    this.id = id;
    this.name = name;
    this.categoryId = categoryId;
    this.saleDate = saleDate;
    this.qulity = qulity;
    this.isDelete = isDelete;
  }
}
//bai10
function listProducts() {
  let publicProduct = [];
  let myProduct1 = new Product(
    1,
    "tuan1",
    2,
    new Date(2022, 7, 15, 3, 40, 50),
    1,
    true
  );
  publicProduct.push(myProduct1);
  let myProduct2 = new Product(
    2,
    "tuan2",
    2,
    new Date(2022, 7, 15, 3, 40, 50),
    2,
    false
  );
  publicProduct.push(myProduct2);
  let myProduct3 = new Product(
    3,
    "tuan3",
    2,
    new Date(2022, 7, 18, 3, 40, 50),
    3,
    true
  );
  publicProduct.push(myProduct3);
  let myProduct4 = new Product(
    4,
    "tuan4",
    3,
    new Date(2022, 7, 17, 3, 40, 50),
    4,
    true
  );
  publicProduct.push(myProduct4);
  let myProduct5 = new Product(
    5,
    "tuan5",
    3,
    new Date(2022, 7, 20, 3, 40, 50),
    -1,
    false
  );
  publicProduct.push(myProduct5);
  let myProduct6 = new Product(
    2,
    "tuan6",
    5,
    new Date(2022, 7, 1, 3, 40, 50),
    6,
    true
  );
  publicProduct.push(myProduct6);
  let myProduct7 = new Product(
    3,
    "tuan7",
    5,
    new Date(2022, 7, 3, 3, 40, 50),
    6,
    false
  );
  publicProduct.push(myProduct7);
  let myProduct8 = new Product(
    8,
    "tuan8",
    27,
    new Date(2022, 7, 9, 3, 40, 50),
    8,
    true
  );
  publicProduct.push(myProduct8);
  let myProduct9 = new Product(
    9,
    "tuan9",
    7,
    new Date(2022, 7, 30, 3, 40, 50),
    -5,
    false
  );
  publicProduct.push(myProduct9);
  let myProduct10 = new Product(10, "tuan10", 27, new Date(), 10, true);
  publicProduct.push(myProduct10);
  return publicProduct;
}
let listProductArray = listProducts();
//bai 11
function fiterProductByIdES6(listProduct, idProduct) {
  let result = listProduct.filter((item) => item.id === idProduct);
  return result;
}
fiterProductByIdES6(listProductArray, 2);

function fiterProductById(listProduct, idProduct) {
  let newListProduct = [];
  for (let i = 0; i < listProduct.length; i++) {
    if (listProduct[i].id === idProduct) {
      newListProduct.push(listProduct[i]);
    }
  }
  return newListProduct;
}
fiterProductById(listProductArray, 3);
//bai12
function fiterProductByQulityES6(listProduct) {
  let result = listProduct.filter(
    (item) => item.qulity > 0 && item.isDelete === false
  );
  return result;
}
fiterProductByQulityES6(listProductArray);
function fiterProductByQulity(listProduct) {
  let newListProduct = [];
  for (let index = 0; index < listProduct.length; index++) {
    if (
      listProduct[index].qulity > 0 &&
      listProduct[index].isDelete === false
    ) {
      newListProduct.push(listProduct[index]);
    }
  }
  return newListProduct;
}

fiterProductByQulity(listProductArray);

//bai 13
function fiterProductBySaleDateES613(listProduct) {
  const newDate = new Date().getDate();
  let result = listProduct.filter(
    (item) => item.saleDate.getDate() > newDate && item.isDelete === false
  );
  return result;
}

fiterProductBySaleDateES613(listProductArray);

function fiterProductBySaleDate13(listProduct) {
  const newDate = new Date().getDate();
  let newListProduct = [];
  for (let index = 0; index < listProduct.length; index++) {
    let dateFromListPro = listProduct[index].saleDate.getDate();
    if (dateFromListPro > newDate && listProduct[index].isDelete === false) {
      newListProduct.push(listProduct[index]);
    }
  }
  return newListProduct;
}
fiterProductBySaleDate13(listProductArray);
// bai 14

function totalProductES6(listProduct) {
  let a = listProduct.filter(
    (item) => item.isDelete === false && item.qulity > 0
  );

  const sumWithInitial = a.reduce((previousValue, currentValue) => {
    if (currentValue.isDelete === false && currentValue.qulity > 0) {
      return previousValue + currentValue.qulity;
    }
  }, 0);
  return sumWithInitial;
}
totalProductES6(listProductArray);

function totalProduct(listProduct) {
  let total = 0;

  for (let index = 0; index < listProduct.length; index++) {
    if (
      listProduct[index].isDelete === false &&
      listProduct[index].qulity > 0
    ) {
      total = total + listProduct[index].qulity;
    }
  }
  return total;
}
totalProduct(listProductArray);
// bai 15
function isHaveProductInCategory(listProduct, categoryID) {
  for (let product of listProduct) {
    if (product.categoryID === categoryID) {
      return true;
    }
  }
  return false;
}

function isHaveProductInCategoryES6(listProduct, categoryID) {
  let a = listProduct.some((product) => product.categoryId === categoryID);
  return a;
}
isHaveProductInCategoryES6(listProductArray, 2);

//bai 16
function fiterProductBySaleDateES6(listProduct) {
  const newDate = new Date().getDate();
  let result = [];
  listProduct.map((item) => {
    if (item.saleDate.getDate() > newDate && item.qulity > 0) {
      result.push(item.id, item.name);
    }
  });

  return JSON.stringify(result);
}
console.log(fiterProductBySaleDateES6(listProductArray));

function fiterProductBySaleDate(listProduct) {
  const newDate = new Date().getDate();
  let result = [];
  for (let index = 0; index < listProduct.length; index++) {
    if (
      listProduct[index].saleDate.getDate() > newDate &&
      listProduct[index].qulity > 0
    ) {
      result.push(listProduct[index].id, listProduct[index].name);
    }
  }

  return JSON.stringify(result);
}

console.log(fiterProductBySaleDate(listProductArray));
